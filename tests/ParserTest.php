<?php
    namespace Tests;

    use Carbon\Carbon;
    use PHPUnit\Framework\TestCase as BaseTestCase;
    use Pb\Parser as PbParser;
    use Pb\Exception\InvalidArgument as InvalidArgumentException;

    class ParserTest extends BaseTestCase {
        protected function _get_test_data() {
            $postcode_ten_downing_street    = 'SW1A 2AA';
            $postcode_mi5                   = 'SW1P 1AE';
            $postcode_mi6                   = 'SE1 1BD';

            return [
                hash('sha256', $postcode_mi5) . ', 2019-01-19T13:12:11, 10.00',
                hash('sha256', $postcode_ten_downing_street) . ', 2019-01-20T13:12:11, 10.00',
                hash('sha256', $postcode_mi5) . ', 2019-01-21T13:12:11, 10.00',
                hash('sha256', $postcode_ten_downing_street) . ', 2019-01-29T12:12:10, 20.00',
                hash('sha256', $postcode_ten_downing_street) . ', 2019-01-29T12:42:11, 10.00',
                hash('sha256', $postcode_mi5) . ', 2019-01-29T13:10:11, 20.00',
                hash('sha256', $postcode_ten_downing_street) . ', 2019-01-29T13:12:11, 10.00', //
                hash('sha256', $postcode_ten_downing_street) . ', 2019-02-02T13:00:00, 10.00',
                hash('sha256', $postcode_mi6) . ', 2019-02-04T13:12:11, 10.00'
            ];
        }        

        public function test_convert_application_datestring_to_date() {
            $first_record_index = 0;

            $converted_application = PbParser::convert_application_datestring_to_date(explode(', ', $this->_get_test_data()[$first_record_index]));

            $this->assertInstanceOf(Carbon::class, $converted_application[1]);
        }

        public function test_convert_application_element_strings_to_arrays() {
            $converted_applications = PbParser::convert_application_element_strings_to_arrays($this->_get_test_data());

            foreach ($converted_applications as $converted_application) {
                $this->assertInternalType('array', $converted_application);
            }
        }

        public function test_filter_applications_by_hash() {

            $postcode_mi5 = 'SW1P 1AE';

            $applications = $this->_get_test_data();

            $applications = PbParser::convert_application_element_strings_to_arrays($applications);

            $applications = PbParser::filter_applications_by_hash($applications, hash('sha256', $postcode_mi5));

            foreach ($applications as $application) {
                $this->assertEquals(hash('sha256', $postcode_mi5), $application[PbParser::APPLICATION_INDEX_HASH]);
            }
        }

        public function test_filter_applications_by_date() {
            $from_date = '2019-01-29T12:42:11';

            $from_date = Carbon::parse($from_date);

            $applications = $this->_get_test_data();

            $applications = PbParser::convert_application_element_strings_to_arrays($applications);

            $applications = PbParser::filter_applications_by_date($applications, $from_date);

            foreach ($applications as $application) {
                $this->assertTrue($from_date->lessThan($application[PbParser::APPLICATION_INDEX_DATE]));
            }
        }

        public function test_get_application_total_within_24_hours() {
            $postcode_ten_downing_street    = 'SW1A 2AA';

            $applications = $this->_get_test_data();

            $applications = PbParser::convert_application_element_strings_to_arrays($applications);

            $postcode_hash = hash('sha256', $postcode_ten_downing_street);

            $total_24hrs = PbParser::get_application_total_within_24_hours($applications, $postcode_hash, 20, Carbon::parse('2019-01-29T13:12:11'));

            $this->assertEquals(70, $total_24hrs);
        }

        public function test_find_throws_invalid_argument_exception() {
            $this->expectException(InvalidArgumentException::class);

            $fraudulent_applications = PbParser::find_fraudulent_applications($this->_get_test_data(), 'nan');
        }
        
        public function test_find_finds_one_fraudulent_application() {
            $fraudulent_applications = PbParser::find_fraudulent_applications($this->_get_test_data(), 31);

            $this->assertCount(1, $fraudulent_applications);

            $postcode_ten_downing_street = 'SW1A 2AA';

            $this->assertEquals(hash('sha256', $postcode_ten_downing_street), $fraudulent_applications[0]);
        }

        public function test_find_finds_zero_fraudulent_application() {
            $fraudulent_applications = PbParser::find_fraudulent_applications($this->_get_test_data(), 41);

            $this->assertCount(0, $fraudulent_applications);
        }        
    }