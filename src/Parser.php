<?php
    namespace Pb;

    use Carbon\Carbon;
    use Pb\Exception\InvalidArgument as InvalidArgumentException;

    class Parser {
        const APPLICATION_INDEX_HASH    = 0;

        const APPLICATION_INDEX_DATE    = 1;

        const APPLICATION_INDEX_AMOUNT  = 2;

        public static function convert_application_datestring_to_date(Array $application) {
            $application[static::APPLICATION_INDEX_DATE] = Carbon::parse($application[static::APPLICATION_INDEX_DATE]);

            return $application;
        }

        public static function convert_application_element_strings_to_arrays(Array $applications) {
            return array_map(function($application) {
                return static::convert_application_datestring_to_date(explode(', ', $application));
            }, $applications);
        }

        public static function filter_applications_by_hash($applications, $hash) {
            return array_filter($applications, function($application) use ($hash) {
                return $application[static::APPLICATION_INDEX_HASH] === $hash;
            });
        }

         public static function filter_applications_by_date($applications, $from_datetime) {
            return array_filter($applications, function($application) use ($from_datetime) {
                return $application[static::APPLICATION_INDEX_DATE]->greaterThan($from_datetime);
            });
        }

        public static function get_application_total_within_24_hours($applications, $postcode_hash, $start_amount, $till_datetime) {
            $from_datetime = clone $till_datetime;
            $from_datetime->subDay();

            $within_24hrs_total = $start_amount;

            $applications = static::filter_applications_by_hash($applications, $postcode_hash);

            $applications = static::filter_applications_by_date($applications, $from_datetime);

            foreach ($applications as $application) {
                $within_24hrs_total += $application[static::APPLICATION_INDEX_AMOUNT];
            }

            return $within_24hrs_total;
        }

        public static function find_fraudulent_applications(Array $applications, $limit_24hrs) {
            $output_fraud = [];

            if (!is_numeric($limit_24hrs)) {
                throw new InvalidArgumentException('$limit_24hrs must be numeric');
            }

            $limit_24hrs = floatval($limit_24hrs);

            $applications = static::convert_application_element_strings_to_arrays($applications);

            for ($i = 0; $i < count($applications); $i++) {
                $total_for_previous_24hrs = static::get_application_total_within_24_hours(
                    array_slice($applications, 0, $i), 
                    $applications[$i][static::APPLICATION_INDEX_HASH], 
                    $applications[$i][static::APPLICATION_INDEX_AMOUNT], 
                    $applications[$i][static::APPLICATION_INDEX_DATE]
                );

                if ($total_for_previous_24hrs >= $limit_24hrs) {
                    $fraudulant_hashed_postcode = $applications[$i][static::APPLICATION_INDEX_HASH];

                    if (!in_array($fraudulant_hashed_postcode, $output_fraud)) {
                        array_push($output_fraud, $fraudulant_hashed_postcode);
                    }
                }
            }

            return $output_fraud;
        }
    }