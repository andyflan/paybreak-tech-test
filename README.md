# Paybreak tech test

## To install for development

1. Clone the repo into the current directory: `git clone git@gitlab.com:andyflan/paybreak-tech-test.git .`
2. Install the dependencies: `composer install`

## To run the tests

Run `vendor/bin/phpunit`

## To use

```
use Pb\Parser as PbParser;

$data = [
    "46f456116352c1753585de70f4c52ca9871bdce2ea4c245ab768bd1071d40dfa, 2019-01-19T13:12:11, 10.00",
    "4508923a98d8e35be8a4d9db69076618a60139b34b65c968e9e8dd0a65d3ce53, 2019-01-20T13:12:11, 10.00",
    "46f456116352c1753585de70f4c52ca9871bdce2ea4c245ab768bd1071d40dfa, 2019-01-21T13:12:11, 10.00",
    "4508923a98d8e35be8a4d9db69076618a60139b34b65c968e9e8dd0a65d3ce53, 2019-01-29T12:12:11, 20.00",
    "4508923a98d8e35be8a4d9db69076618a60139b34b65c968e9e8dd0a65d3ce53, 2019-01-29T12:42:11, 10.00",
    "4508923a98d8e35be8a4d9db69076618a60139b34b65c968e9e8dd0a65d3ce53, 2019-01-29T13:12:11, 10.00",
    "4508923a98d8e35be8a4d9db69076618a60139b34b65c968e9e8dd0a65d3ce53, 2019-02-02T13:12:11, 10.00",
    "a6497a2ac14c14948630e75a64e8aff17a08525ea6b5189bb1ca4d7098b78ea3, 2019-02-04T13:12:11, 10.00"
);

$parser = new PbParser();

$fraudulent_applications = $parser->find_fraudulent_applications($data, 41);

```

## A few dev notes

1. All the methods of the `Pb\Parser` class are public, so that tests can be written for them.
2. I've tried to make this as easy to read and maintain as possible by writing clear code and by supplying tests, rather than by adding lots of comments.